﻿namespace cv_04
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.gameListBox = new System.Windows.Forms.ListBox();
            this.gameStatusStrip = new System.Windows.Forms.StatusStrip();
            this.dfficultProgressBar = new System.Windows.Forms.ToolStripProgressBar();
            this.difficultLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.accurancyLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.missedLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.correctLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItemNewGame = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemAbout = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemGameMode = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripComboBoxGameMode = new System.Windows.Forms.ToolStripComboBox();
            this.gameStatusStrip.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // gameListBox
            // 
            this.gameListBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gameListBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 99.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.gameListBox.ItemHeight = 152;
            this.gameListBox.Location = new System.Drawing.Point(0, 0);
            this.gameListBox.MultiColumn = true;
            this.gameListBox.Name = "gameListBox";
            this.gameListBox.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.gameListBox.Size = new System.Drawing.Size(911, 187);
            this.gameListBox.TabIndex = 0;
            this.gameListBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.gameListBox_KeyDown);
            // 
            // gameStatusStrip
            // 
            this.gameStatusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dfficultProgressBar,
            this.difficultLabel,
            this.accurancyLabel,
            this.missedLabel,
            this.correctLabel});
            this.gameStatusStrip.Location = new System.Drawing.Point(0, 165);
            this.gameStatusStrip.Name = "gameStatusStrip";
            this.gameStatusStrip.Size = new System.Drawing.Size(911, 22);
            this.gameStatusStrip.TabIndex = 1;
            this.gameStatusStrip.Text = "statusStrip1";
            // 
            // dfficultProgressBar
            // 
            this.dfficultProgressBar.Maximum = 800;
            this.dfficultProgressBar.Name = "dfficultProgressBar";
            this.dfficultProgressBar.Size = new System.Drawing.Size(100, 16);
            // 
            // difficultLabel
            // 
            this.difficultLabel.Name = "difficultLabel";
            this.difficultLabel.Size = new System.Drawing.Size(595, 17);
            this.difficultLabel.Spring = true;
            this.difficultLabel.Text = "Difficult";
            this.difficultLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // accurancyLabel
            // 
            this.accurancyLabel.Name = "accurancyLabel";
            this.accurancyLabel.Size = new System.Drawing.Size(85, 17);
            this.accurancyLabel.Text = "Accurancy: 0%";
            // 
            // missedLabel
            // 
            this.missedLabel.Name = "missedLabel";
            this.missedLabel.Size = new System.Drawing.Size(56, 17);
            this.missedLabel.Text = "Missed: 0";
            // 
            // correctLabel
            // 
            this.correctLabel.Name = "correctLabel";
            this.correctLabel.Size = new System.Drawing.Size(58, 17);
            this.correctLabel.Text = "Correct: 0";
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 800;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItemNewGame,
            this.toolStripMenuItemAbout,
            this.toolStripMenuItemGameMode});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.menuStrip1.Size = new System.Drawing.Size(911, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // toolStripMenuItemNewGame
            // 
            this.toolStripMenuItemNewGame.Name = "toolStripMenuItemNewGame";
            this.toolStripMenuItemNewGame.Size = new System.Drawing.Size(79, 20);
            this.toolStripMenuItemNewGame.Text = "New game!";
            this.toolStripMenuItemNewGame.Click += new System.EventHandler(this.toolStripMenuItemNewGame_Click);
            // 
            // toolStripMenuItemAbout
            // 
            this.toolStripMenuItemAbout.Name = "toolStripMenuItemAbout";
            this.toolStripMenuItemAbout.Size = new System.Drawing.Size(52, 20);
            this.toolStripMenuItemAbout.Text = "About";
            this.toolStripMenuItemAbout.Click += new System.EventHandler(this.toolStripMenuItemAbout_Click);
            // 
            // toolStripMenuItemGameMode
            // 
            this.toolStripMenuItemGameMode.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripComboBoxGameMode});
            this.toolStripMenuItemGameMode.Name = "toolStripMenuItemGameMode";
            this.toolStripMenuItemGameMode.Size = new System.Drawing.Size(84, 20);
            this.toolStripMenuItemGameMode.Text = "Game mode";
            // 
            // toolStripComboBoxGameMode
            // 
            this.toolStripComboBoxGameMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.toolStripComboBoxGameMode.Items.AddRange(new object[] {
            "Frist",
            "All"});
            this.toolStripComboBoxGameMode.Name = "toolStripComboBoxGameMode";
            this.toolStripComboBoxGameMode.Size = new System.Drawing.Size(121, 23);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(911, 187);
            this.Controls.Add(this.gameStatusStrip);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.gameListBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Text = "Cviceni 04";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.gameStatusStrip.ResumeLayout(false);
            this.gameStatusStrip.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox gameListBox;
        private System.Windows.Forms.StatusStrip gameStatusStrip;
        private System.Windows.Forms.ToolStripProgressBar dfficultProgressBar;
        private System.Windows.Forms.ToolStripStatusLabel difficultLabel;
        private System.Windows.Forms.ToolStripStatusLabel accurancyLabel;
        private System.Windows.Forms.ToolStripStatusLabel missedLabel;
        private System.Windows.Forms.ToolStripStatusLabel correctLabel;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemNewGame;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemAbout;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemGameMode;
        private System.Windows.Forms.ToolStripComboBox toolStripComboBoxGameMode;
    }
}

