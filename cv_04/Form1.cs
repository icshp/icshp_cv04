﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace cv_04
{
    public partial class Form1 : Form
    {

        private Random random;
        private Stats stats;
        private bool gameMode;

        public Form1()
        {
            InitializeComponent();
            random = new Random();
            stats = new Stats();
            stats.UpdatedStats += Stats_UpdatedStats;
            gameListBox.Items.Clear();
            toolStripComboBoxGameMode.SelectedIndex = 1;
        }

        private void Stats_UpdatedStats(object sender, EventArgs e)
        {
            correctLabel.Text = "Correct: " + stats.Correct.ToString();
            missedLabel.Text = "Missed: " + stats.Missed.ToString();
            accurancyLabel.Text = "Accuracy: " + stats.Accuracy.ToString();
        
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            gameListBox.Items.Add((Keys)random.Next('A', 'Z'));
            if (gameListBox.Items.Count > 6) {
                timer1.Stop();
                gameListBox.Items.Clear();
                gameListBox.Items.Add("GAME OVER");
            }
        }

        private void gameListBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (toolStripComboBoxGameMode.SelectedIndex == 1)
            {
                if (gameListBox.Items.Contains(e.KeyCode))
                {
                    gameListBox.Items.Remove(e.KeyCode);
                    gameListBox.Refresh();
                    stats.Update(true);
                }
                else
                {
                    stats.Update(false);
                }
            }
            else {
                if (gameListBox.Items[0].ToString().Contains(e.KeyCode.ToString()))
                {
                    gameListBox.Items.Remove(e.KeyCode);
                    gameListBox.Refresh();
                    stats.Update(true);
                }
                else
                {
                    stats.Update(false);
                }
            }
          

            if (timer1.Interval > 400)
            {
                timer1.Interval -= 60;
            }
            else if (timer1.Interval > 250)
            {
                timer1.Interval -= 15;
            }
            else if (timer1.Interval > 150)
            {
                timer1.Interval -= 8;
            }

            dfficultProgressBar.Value = 800 - timer1.Interval;
        }

        private void toolStripMenuItemNewGame_Click(object sender, EventArgs e)
        {
            start();
        }

        private void start() {
            stats.Restart();
            gameListBox.Items.Clear();
            timer1.Interval = 800;
            stats.OnUpdatedStats();
            timer1.Start();
        }

        private void toolStripMenuItemAbout_Click(object sender, EventArgs e)
        {
            timer1.Stop();
            MessageBox.Show("Hra je hraje pomocí klávesnice a tvým úkolem je mačkat stejná písmena jako vidíš na obrazovce. " +
                "Pokud překročíš 6 znaků, prohrál jsi.",
                "Informace o hře",
                MessageBoxButtons.OK,
                MessageBoxIcon.Information);
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}

