﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cv_04
{

    public delegate void UpdatedStatsEventHandler(object sender, EventArgs e);

    class Stats
    {

        public int Correct { get; private set; }

        public int Missed { get; private set; }

        public double Accuracy { get; private set; }

        public event UpdatedStatsEventHandler UpdatedStats;

        public void OnUpdatedStats()
        {
            UpdatedStatsEventHandler handler = UpdatedStats;
            if (handler != null)
                handler(this, new EventArgs());
        }

        public void Update(bool correctKey) {
            if (correctKey)
            {
                Correct++;
            }
            else {
                Missed++;
            }

            if ((Missed + Correct) > 0 && Correct > 0) {
                Accuracy = (double) Correct / (Correct + Missed);
            }

            OnUpdatedStats();
        }

        public void Restart() {
            Correct = Missed = 0;
            Accuracy = 1;
        }

    }
}
